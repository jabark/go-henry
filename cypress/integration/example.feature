Feature: Example Feature

  An example for a feature

  @focus
  Scenario: Opening the homepage
    Given I open the homepage
    Then I see the welcome page

  @focus
  Scenario: Next Steps
    Given I open the homepage
    When I click the "Angular Material" button
    Then I see a command to add Angular Material
    When I click the "New Component" button
    Then I see a command to generate a new component
