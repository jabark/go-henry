import { Given, Then, When } from 'cypress-cucumber-preprocessor/steps';

Given('I open the homepage', () => {
  cy.visit('/');
});

Then('I see the welcome page', () => {
  cy.get('.toolbar').contains('Welcome');
});

When('I click the "Angular Material" button', () => {
  cy.contains('Angular Material').click();
});

Then('I see a command to add Angular Material', () => {
  cy.get('.terminal').contains('ng add @angular/material');
});

When('I click the "New Component" button', () => {
  cy.contains('New Component').click();
});

Then('I see a command to generate a new component', () => {
  cy.get('.terminal').contains('ng generate component xyz');
});
