# Go Henry Tech Test by Jamie Barker

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.0.3.

## Setup instructions

The app was created using NodeJS version 16.13.0 so ideally use that but it _probably_ will work with anything close to that version. If you have NVM installed on UNIX based machine then it should pick this up already and use it, however if you use a Windows machine like me then you can run `npm run win:nvm:install`. You will need [NVM for Windows](https://github.com/coreybutler/nvm-windows) installed though.

Once you're happy with your NodeJS setup simply run `npm i` to install the required packages then you should be in a position to run the app.

## Testing the app

Run `npm run start` and then navigate to `http://localhost:2222/`.

It wasn't mentioned, but I have added in some error handling for if/when the API requests fail. If you want to test this out just simply change the `url` property in the museum service (I simply added an "x" to the end). 

As I also feel it's important for code to be consistent, I've also got Prettify, ESLint and Stylelint all set up, not necessarily how I would normally have it (I think some things got messed up when versions were upgraded) but you can run these by doing `npm run lint`.

## Running unit tests

Run `npm run test` to execute the unit tests that have been written in Jest. A `coverage` directory will be created when it has finished and within it an `index.html` file can be opened in your web browser to see the code coverage.

## Further information

- Cypress was added to the project and the intention was to add e2e tests however time has got the better of me. If you go back to the `add boilerplate stuff` commit you can run them and they will work against the app component template supplied when you create a new Angular application by running `npm run cypress`.
- I've used Angular Material to make it so that I didn't need to recreate things like modal dialogs and buttons.
- I wasn't sure what was expected to be done with the objects that didn't have images. I figured it would have been ideal to ignore the ones without images, however this proved problematic due to me needing to lazy load the objects and not knowing whether they have images until I already requested them individually. I decided it would probably be best to follow what MetMuseum themselves did and just show a placeholder. I also decided to temporarily use their own placeholder SVG to save myself some time in finding something else.
- To make sure code quality is good and prevent bad things leaking through, I've got some commit hooks set up to run linting and unit tests
- Things I haven't had chance to do:
  - The search functionality. I was planning to make the search bar run the search http request a short period of time after you've finished typing, but only after 3 characters have been entered. So for example if you've typed "picasso", about a second after you stop typing it would run the `/public/collection/v1/search?q=picasso` GET request and show the results on the page. For simplicity I would probably have just made it another department at the top called "Search Results for 'Picasso'" as the data structure returned is the same. If you'd still like me to complete this then I'm happy to do it over the weekend, just let me know.
  - Unit tests. I've managed to get _most_ unit tests completed, however writing tests for the monster http request setup would probably take more time than necessary for the purpose of demonstrating how I tackle unit testing. Just let me know if you'd still like me to complete them but I'll just need more time.
  - e2e tests. As mentioned above, I would have liked to have done some e2e tests as I feel they are also important in the development lifecycle, I just unfortunately forgot about them until it was too late.
  - Find a better way of doing the HTTP requests. I'll be the first to admit that the museum service I've written is a mess. Trying to use the Metmuseum API proved rather challenging considering the crazy amount of HTTP requests that are required in order to get all the required data to display on the screen. It's annoying as I feel like there's a much cleaner way of achieving it but as I'd already gone round the houses to get to the point it is now (I had initially set it up for each request to be separate but that was taking an extortionate amount of time), I didn't have enough time left to look into improving it further. There's probably an RxJS operator I haven't thought of...
- Amount of time spent:
  - I haven't been tracking how long I've spent working on this but I would say it's approximately 6-8 hours over a couple of days, although I'm not counting the time up to the point I did the "add boilerplate stuff" commit. To explain briefly why that is, I have a boilerplate empty Angular app that's about a year old in my personal BitBucket that has the things like Jest, ESLint, Cypress pre-installed, however getting these things to work nicely with Angular 13 was a _lot_ more painful than I anticipated.
  - I also found that quite a few of my previous ways of doing things like subscriptions were deprecated in latest version of RxJS and it took a while to get that all set up and working.
  - Some things also took longer because the computer I'm using is a new build so I had to set everything up again. One thing in particular that didn't help at all was the fact that for some reason VSCode wasn't highlighting errors and issues before I ran into them like it does on my work machine and it was making it take longer to get past issues. Felt a bit like I was working with IE6 again! Thankfully it seems to have started working again today after I think what was a VSCode update yesterday as I was switching off, so I can only assume it was that. 

If there's anything else you would like to know or need assistance with, drop me a line at my email address below

- Name: Jamie Barker
- Email: jaadba@gmail.com
- Role: Front End Engineering Lead

Update 28/11: Have managed to refactor services to better make use of RxJS as I was wanting and added the search feature
