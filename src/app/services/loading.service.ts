import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoadingService {
  departmentLoading = new BehaviorSubject(0);
  isLoading = new BehaviorSubject(false);
}
