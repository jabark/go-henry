import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { when } from 'jest-when';
import { BehaviorSubject, of as ObservableOf } from 'rxjs';

import { IDepartment } from '../models/departments';
import { IObject } from '../models/objects';
import { LoadingService } from './loading.service';
import { MuseumService } from './museum.service';
import { SearchService } from './search.service';

describe('SearchService', () => {
  let service: SearchService;

  const baseUrl = 'https://collectionapi.metmuseum.org/public/collection/v1';
  const testDepartments: IDepartment[] = [
    {
      departmentId: 22,
      displayName: 'Best Department',
      objectData: {
        total: 14,
        objectIDs: [1, 5, 9, 10, 13, 20, 22, 23, 24, 25, 26, 1337, 1339, 1420],
      },
      objects: [{ objectID: 1 }, { objectID: 5 }, { objectID: 9 }, { objectID: 10 }, { objectID: 13 }, { objectID: 20 }] as IObject[],
    },
    {
      departmentId: 1337,
      displayName: 'Elite Department',
      objectData: {
        total: 6,
        objectIDs: [42, 43, 44, 45, 46, 47],
      },
      objects: [{ objectID: 42 }, { objectID: 43 }, { objectID: 44 }, { objectID: 45 }, { objectID: 46 }, { objectID: 47 }] as IObject[],
    },
  ];
  const mockHttpClient = {
    get: jest.fn(),
  };
  const mockMuseumService = {
    $getDepartments: jest.fn().mockReturnValue(ObservableOf(testDepartments)),
    setDepartments: jest.fn(),
    url: baseUrl,
  };
  const testSearchDepartment: IDepartment = {
    departmentId: -1,
    displayName: 'Search results for "Awesome"',
    objectData: {
      total: 2,
      objectIDs: [22, 1337],
    },
    objects: [{ objectID: 22 }, { objectID: 1337 }] as IObject[],
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: HttpClient,
          useValue: mockHttpClient,
        },
        {
          provide: MuseumService,
          useValue: mockMuseumService,
        },
        {
          provide: LoadingService,
          useValue: {
            departmentLoading: new BehaviorSubject(0),
          },
        },
      ],
    });

    service = TestBed.inject(SearchService);
  });

  describe('$getSearchTerm()', () => {
    it('should return _searchTerm', (done) => {
      (service as any)._searchTerm = new BehaviorSubject('Picasso');
      service.$getSearchTerm().subscribe((res: any) => {
        expect(res).toEqual('Picasso');
        done();
      });
    });
  });

  describe('setSearchTerm()', () => {
    it('should update the searchterm observable', () => {
      const nextSpy = jest.spyOn((service as any)._searchTerm, 'next');

      service.setSearchTerm('Watercolor');

      expect(nextSpy).toHaveBeenCalledWith('Watercolor');
    });
  });

  describe('$search()', () => {
    it('should return with a search department', (done) => {
      when((service as any).http.get)
        .calledWith(`${baseUrl}/search?q=Awesome`)
        .mockReturnValue(ObservableOf(testSearchDepartment.objectData));
      when((service as any).http.get)
        .calledWith(`${baseUrl}/objects/22`)
        .mockReturnValue(ObservableOf(testSearchDepartment.objects[0]));
      when((service as any).http.get)
        .calledWith(`${baseUrl}/objects/1337`)
        .mockReturnValue(ObservableOf(testSearchDepartment.objects[1]));

      service.$search('Awesome').subscribe((res) => {
        expect(res).toEqual(testSearchDepartment);
        done();
      });
    });
  });

  describe('setSearchDepartment()', () => {
    afterAll(() => {
      ((service as any).museumService.$getDepartments as jest.SpyInstance).mockReset();
    });

    it('should call setDepartments with an added search department', (done) => {
      ((service as any).museumService.$getDepartments as jest.SpyInstance).mockReturnValue(ObservableOf(testDepartments));

      service.setSearchDepartment(testSearchDepartment);

      (service as any).museumService.$getDepartments().subscribe(() => {
        expect((service as any).museumService.setDepartments).toHaveBeenCalledWith([
          testSearchDepartment,
          testDepartments[0],
          testDepartments[1],
        ]);
        done();
      });
    });
  });

  describe('removeSearchDepartment()', () => {
    afterAll(() => {
      ((service as any).museumService.$getDepartments as jest.SpyInstance).mockReset();
    });

    it('should call setDepartments with search department removed', (done) => {
      ((service as any).museumService.$getDepartments as jest.SpyInstance).mockReturnValue(
        ObservableOf([testSearchDepartment, testDepartments[0], testDepartments[1]]),
      );

      service.removeSearchDepartment();

      (service as any).museumService.$getDepartments().subscribe(() => {
        expect((service as any).museumService.setDepartments).toHaveBeenCalledWith(testDepartments);
        done();
      });
    });
  });

  describe('$hasSearchDepartment()', () => {
    afterAll(() => {
      ((service as any).museumService.$getDepartments as jest.SpyInstance).mockReset();
    });

    it('should return with an boolean observable whether departments has a search observable', (done) => {
      ((service as any).museumService.$getDepartments as jest.SpyInstance).mockReturnValue(ObservableOf(testDepartments));

      service.$hasSearchDepartment().subscribe((res) => {
        expect(res).toBe(false);
      });

      ((service as any).museumService.$getDepartments as jest.SpyInstance).mockReturnValue(
        ObservableOf([testSearchDepartment, testDepartments[0], testDepartments[1]]),
      );

      service.$hasSearchDepartment().subscribe((res) => {
        expect(res).toBe(true);
        done();
      });
    });
  });

  describe('updateSearchDepartment()', () => {
    afterAll(() => {
      ((service as any).museumService.$getDepartments as jest.SpyInstance).mockReset();
    });

    it('should  call setDepartments with search department updated', (done) => {
      ((service as any).museumService.$getDepartments as jest.SpyInstance).mockReturnValue(
        ObservableOf([testSearchDepartment, testDepartments[0], testDepartments[1]]),
      );
      const newSearchDepartment: IDepartment = {
        departmentId: -1,
        displayName: 'Search results for "Nines"',
        objectData: {
          total: 2,
          objectIDs: [99, 999],
        },
        objects: [{ objectID: 99 }, { objectID: 999 }] as IObject[],
      };

      service.updateSearchDepartment(newSearchDepartment);

      (service as any).museumService.$getDepartments().subscribe(() => {
        expect((service as any).museumService.setDepartments).toHaveBeenCalledWith([
          newSearchDepartment,
          testDepartments[0],
          testDepartments[1],
        ]);
        done();
      });
    });
  });
});
