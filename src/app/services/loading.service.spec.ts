import { TestBed } from '@angular/core/testing';

import { LoadingService } from './loading.service';

describe('LoadingService', () => {
  let service: LoadingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LoadingService);
  });

  it('should set defaults', () => {
    service.isLoading.subscribe((isLoading) => {
      expect(isLoading).toBe(false);
    });

    service.departmentLoading.subscribe((departmentLoading) => {
      expect(departmentLoading).toBe(0);
    });
  });
});
