import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { INotification, ENotificationType } from '../models/notifications';

@Injectable({
  providedIn: 'root',
})
export class NotificationService {
  private readonly defaultNotification: INotification = {
    title: '',
    type: ENotificationType.NONE,
  };

  private readonly _notification: BehaviorSubject<INotification> = new BehaviorSubject<INotification>(this.defaultNotification);

  get notification(): Observable<INotification> {
    return this._notification.asObservable();
  }

  setNotification(notification: INotification): void {
    this._notification.next(notification);
  }

  removeNotification(): void {
    this.setNotification(this.defaultNotification);
  }
}
