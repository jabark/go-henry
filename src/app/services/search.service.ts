import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, combineLatestAll, of as ObservableOf, map, mergeMap, Observable, take } from 'rxjs';

import { IDepartment } from '../models/departments';
import { IObjects } from '../models/objects';
import { LoadingService } from './loading.service';
import { MuseumService } from './museum.service';

@Injectable({
  providedIn: 'root',
})
export class SearchService {
  private readonly _searchTerm: BehaviorSubject<string> = new BehaviorSubject<string>('');

  constructor(
    private readonly loadingService: LoadingService,
    private readonly http: HttpClient,
    private readonly museumService: MuseumService,
  ) {}

  $getSearchTerm(): Observable<string> {
    return this._searchTerm.asObservable();
  }

  setSearchTerm(value: string): void {
    this._searchTerm.next(value);
  }

  $search(searchTerm: string): Observable<any> {
    this.loadingService.departmentLoading.next(-1);
    const displayName = `Search results for "${searchTerm}"`;
    let objectData: IObjects;
    return this.http.get(`${this.museumService.url}/search?q=${searchTerm}`).pipe(
      mergeMap((objects: any) => {
        objectData = objects;
        const $objs: Observable<any>[] = [];
        if ((objectData as IObjects).objectIDs?.length) {
          (objectData as IObjects).objectIDs.forEach((objectId: number, index2: number) => {
            if (index2 < 6) {
              $objs.push(
                this.http.get(`${this.museumService.url}/objects/${objectId}`).pipe(
                  catchError((err) => {
                    return this.museumService.pipeError(err, `Object could not be retrieved for Object with ID of ${objectId}`);
                  }),
                ),
              );
            }
          });

          return $objs;
        }

        return [ObservableOf({})];
      }),
      combineLatestAll(),
      map((objects) => {
        return {
          departmentId: -1,
          displayName,
          objectData,
          objects,
        } as IDepartment;
      }),
      catchError((err) => {
        return this.museumService.pipeError(err, `Objects information could not be retrieved in search for "${searchTerm}"`);
      }),
    );
  }

  setSearchDepartment(dept: IDepartment): void {
    this.museumService
      .$getDepartments()
      .pipe(take(1))
      .subscribe((departments) => {
        const newDepts = JSON.parse(JSON.stringify(departments));
        newDepts.unshift(dept);
        this.museumService.setDepartments(newDepts);
      });
  }

  removeSearchDepartment(): void {
    this.museumService
      .$getDepartments()
      .pipe(take(1))
      .subscribe((departments) => {
        const newDepts = JSON.parse(JSON.stringify(departments)).filter((dept: IDepartment) => {
          return dept.departmentId !== -1;
        });
        this.museumService.setDepartments(newDepts);
      });
  }

  $hasSearchDepartment(): Observable<boolean> {
    return this.museumService.$getDepartments().pipe(
      map((departments) => {
        return !!departments.filter((dept: IDepartment) => {
          return dept.departmentId === -1;
        }).length;
      }),
    );
  }

  updateSearchDepartment(dept: IDepartment): void {
    this.museumService
      .$getDepartments()
      .pipe(take(1))
      .subscribe((departments) => {
        const newDepts = departments;
        newDepts[0] = dept;
        this.museumService.setDepartments(newDepts);
      });
  }
}
