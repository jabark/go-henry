import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { when } from 'jest-when';
import { BehaviorSubject, EMPTY, forkJoin, Observable, of as ObservableOf, tap } from 'rxjs';

import { IDepartment } from '../models/departments';
import { ENotificationType } from '../models/notifications';
import { IObject } from '../models/objects';
import { MuseumService } from './museum.service';
import { NotificationService } from './notification.service';

describe('MuseumService', () => {
  let service: MuseumService;
  let notificationService: NotificationService;

  const testDepartments: IDepartment[] = [
    {
      departmentId: 22,
      displayName: 'Best Department',
      objectData: {
        total: 14,
        objectIDs: [1, 5, 9, 10, 13, 20, 22, 23, 24, 25, 26, 1337, 1339, 1420],
      },
      objects: [{ objectID: 1 }, { objectID: 5 }, { objectID: 9 }, { objectID: 10 }, { objectID: 13 }, { objectID: 20 }] as IObject[],
    },
    {
      departmentId: 1337,
      displayName: 'Elite Department',
      objectData: {
        total: 6,
        objectIDs: [42, 43, 44, 45, 46, 47],
      },
      objects: [{ objectID: 42 }, { objectID: 43 }, { objectID: 44 }, { objectID: 45 }, { objectID: 46 }, { objectID: 47 }] as IObject[],
    },
  ];
  const mockHttpClient = {
    get: jest.fn(),
  };
  const mockNotificationService = {
    setNotification: jest.fn(),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: HttpClient,
          useValue: mockHttpClient,
        },
        {
          provide: NotificationService,
          useValue: mockNotificationService,
        },
      ],
    });

    service = TestBed.inject(MuseumService);
    notificationService = TestBed.inject(NotificationService);
  });

  describe('$getDepartments()', () => {
    it('should return _departments', (done) => {
      (service as any)._departments = new BehaviorSubject(testDepartments);
      service.$getDepartments().subscribe((res: any) => {
        expect(res).toEqual(testDepartments);
        done();
      });
    });
  });

  describe('setDepartments()', () => {
    it('should update the searchterm observable', () => {
      const nextSpy = jest.spyOn((service as any)._departments, 'next');

      service.setDepartments('Some departments' as any);

      expect(nextSpy).toHaveBeenCalledWith('Some departments');
    });
  });

  describe('retrieveDepartments()', () => {
    it('should subscribe to the $retrieveDepartments observable and set the departments', (done) => {
      (service as any).$retrieveDepartments = jest.fn().mockReturnValue(ObservableOf('Some departments'));
      (service as any)._departments.next = jest.fn();

      service.retrieveDepartments();

      (service as any).$retrieveDepartments().subscribe(() => {
        expect((service as any)._departments.next).toHaveBeenCalledWith('Some departments');
        done();
      });
    });
  });

  describe('getNextObjects()', () => {
    const baseUrl = 'https://collectionapi.metmuseum.org/public/collection/v1/objects/';
    it('should do http get requests for the next 6 objects', (done) => {
      when((service as any).http.get)
        .calledWith(`${baseUrl}22`)
        .mockReturnValue(ObservableOf({ objectID: 22 }));
      when((service as any).http.get)
        .calledWith(`${baseUrl}23`)
        .mockReturnValue(ObservableOf({ objectID: 23 }));
      when((service as any).http.get)
        .calledWith(`${baseUrl}24`)
        .mockReturnValue(ObservableOf({ objectID: 24 }));
      when((service as any).http.get)
        .calledWith(`${baseUrl}25`)
        .mockReturnValue(ObservableOf({ objectID: 25 }));
      when((service as any).http.get)
        .calledWith(`${baseUrl}26`)
        .mockReturnValue(ObservableOf({ objectID: 26 }));
      when((service as any).http.get)
        .calledWith(`${baseUrl}1337`)
        .mockReturnValue(ObservableOf({ objectID: 1337 }));
      const testObjectCalls: Observable<any>[] = [
        (service as any).http.get(`${baseUrl}22`),
        (service as any).http.get(`${baseUrl}23`),
        (service as any).http.get(`${baseUrl}24`),
        (service as any).http.get(`${baseUrl}25`),
        (service as any).http.get(`${baseUrl}26`),
        (service as any).http.get(`${baseUrl}1337`),
      ];
      const localTestDepartments = JSON.parse(JSON.stringify(testDepartments));
      (service as any)._departments = new BehaviorSubject<IDepartment[]>(localTestDepartments);
      jest.spyOn((service as any)._departments, 'next');

      service.getNextObjects(localTestDepartments[0], 6);

      forkJoin(testObjectCalls)
        .pipe(
          tap(() => {
            expect((service as any)._departments.next).toHaveBeenCalledWith([
              {
                departmentId: 22,
                displayName: 'Best Department',
                objectData: {
                  total: 14,
                  objectIDs: [1, 5, 9, 10, 13, 20, 22, 23, 24, 25, 26, 1337, 1339, 1420],
                },
                objects: [{ objectID: 22 }, { objectID: 23 }, { objectID: 24 }, { objectID: 25 }, { objectID: 26 }, { objectID: 1337 }],
              },
              localTestDepartments[1],
            ]);
            done();
          }),
        )
        .subscribe();
    });
  });

  describe('pipeError()', () => {
    let returnValue: Observable<never>;

    beforeEach(() => {
      jest.spyOn(console, 'error');
      (service as any).setErrorNotification = jest.fn();

      returnValue = (service as any).pipeError('Some error', 'Error description');
    });

    it('should put the error in the console', () => {
      expect(console.error).toHaveBeenCalledWith('Some error');
    });

    it('should call setErrorNotification with the description', () => {
      expect((service as any).setErrorNotification).toHaveBeenCalledWith('Error description');
    });

    it('should return EMPTY', () => {
      expect(returnValue).toEqual(EMPTY);
    });
  });

  describe('$retrieveDepartments()', () => {
    const url = 'https://collectionapi.metmuseum.org/public/collection/v1';

    it('should return an observable of a full IDepartment[] object', (done) => {
      (service as any)._departments = new BehaviorSubject<IDepartment[]>([]);
      when((service as any).http.get)
        .calledWith(`${url}/departments`)
        .mockReturnValue(
          ObservableOf({
            departments: [
              { departmentId: 22, displayName: 'Best Department' },
              { departmentId: 1337, displayName: 'Elite Department' },
            ],
          }),
        );
      when((service as any).http.get)
        .calledWith(`${url}/objects?departmentIds=22`)
        .mockReturnValue(ObservableOf(testDepartments[0].objectData));
      when((service as any).http.get)
        .calledWith(`${url}/objects?departmentIds=1337`)
        .mockReturnValue(ObservableOf(testDepartments[1].objectData));
      when((service as any).http.get)
        .calledWith(`${url}/objects/1`)
        .mockReturnValue(ObservableOf({ objectID: 1 }));
      when((service as any).http.get)
        .calledWith(`${url}/objects/5`)
        .mockReturnValue(ObservableOf({ objectID: 5 }));
      when((service as any).http.get)
        .calledWith(`${url}/objects/9`)
        .mockReturnValue(ObservableOf({ objectID: 9 }));
      when((service as any).http.get)
        .calledWith(`${url}/objects/10`)
        .mockReturnValue(ObservableOf({ objectID: 10 }));
      when((service as any).http.get)
        .calledWith(`${url}/objects/13`)
        .mockReturnValue(ObservableOf({ objectID: 13 }));
      when((service as any).http.get)
        .calledWith(`${url}/objects/20`)
        .mockReturnValue(ObservableOf({ objectID: 20 }));
      when((service as any).http.get)
        .calledWith(`${url}/objects/42`)
        .mockReturnValue(ObservableOf({ objectID: 42 }));
      when((service as any).http.get)
        .calledWith(`${url}/objects/43`)
        .mockReturnValue(ObservableOf({ objectID: 43 }));
      when((service as any).http.get)
        .calledWith(`${url}/objects/44`)
        .mockReturnValue(ObservableOf({ objectID: 44 }));
      when((service as any).http.get)
        .calledWith(`${url}/objects/45`)
        .mockReturnValue(ObservableOf({ objectID: 45 }));
      when((service as any).http.get)
        .calledWith(`${url}/objects/46`)
        .mockReturnValue(ObservableOf({ objectID: 46 }));
      when((service as any).http.get)
        .calledWith(`${url}/objects/47`)
        .mockReturnValue(ObservableOf({ objectID: 47 }));

      (service as any).$retrieveDepartments().subscribe((data: any) => {
        expect(data).toEqual(testDepartments);
        done();
      });
    });
  });

  describe('setErrorNotification()', () => {
    it('should set the specified error notification', () => {
      (service as any).setErrorNotification('Department information could not be retrieved');

      expect(notificationService.setNotification).toHaveBeenCalledWith({
        description: 'Department information could not be retrieved',
        title: 'Error',
        type: ENotificationType.ERROR,
      });
    });
  });
});
