import { TestBed } from '@angular/core/testing';
import { BehaviorSubject } from 'rxjs';

import { INotification, ENotificationType } from '../models/notifications';
import { NotificationService } from './notification.service';

describe('NotificationService', () => {
  let service: NotificationService;

  const emptyNotification: INotification = {
    title: '',
    type: ENotificationType.NONE,
  };
  const notification: INotification = {
    description: 'Foo Bar',
    title: 'Error',
    type: ENotificationType.ERROR,
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NotificationService],
    });
    service = TestBed.inject(NotificationService);
  });

  describe('get notification()', () => {
    it('should return the notification', () => {
      (service as any)._notification = new BehaviorSubject<INotification>(emptyNotification);
      service.notification.subscribe((data) => {
        expect(data).toEqual(emptyNotification);
      });

      (service as any)._notification = new BehaviorSubject<INotification>(notification);
      service.notification.subscribe((data) => {
        expect(data).toEqual(notification);
      });
    });
  });

  describe('setNotification()', () => {
    beforeEach(() => {
      (service as any)._notification.next = jest.fn();
    });

    it('should set the notification', () => {
      service.setNotification(notification);
      expect((service as any)._notification.next).toHaveBeenCalledWith(notification);

      service.setNotification(emptyNotification);

      expect((service as any)._notification.next).toHaveBeenCalledWith(emptyNotification);
    });
  });

  describe('removeNotification()', () => {
    it('should reset the notification', () => {
      service.setNotification = jest.fn();

      service.removeNotification();

      expect(service.setNotification).toHaveBeenCalledWith(emptyNotification);
    });
  });
});
