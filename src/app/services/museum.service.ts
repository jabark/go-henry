import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, mergeMap, combineLatestAll, map, catchError, EMPTY, forkJoin } from 'rxjs';
import { tap } from 'rxjs/operators';

import { IDepartment } from '../models/departments';
import { ENotificationType } from '../models/notifications';
import { IObjects } from '../models/objects';
import { LoadingService } from './loading.service';
import { NotificationService } from './notification.service';

@Injectable({
  providedIn: 'root',
})
export class MuseumService {
  url = 'https://collectionapi.metmuseum.org/public/collection/v1';
  private readonly _departments: BehaviorSubject<IDepartment[]> = new BehaviorSubject<IDepartment[]>([]);

  constructor(
    private readonly http: HttpClient,
    private readonly notificationService: NotificationService,
    private readonly loadingService: LoadingService,
  ) {}

  $getDepartments(): Observable<IDepartment[]> {
    return this._departments.asObservable();
  }

  setDepartments(departments: IDepartment[]): void {
    this._departments.next(departments);
  }

  retrieveDepartments(): void {
    this.$retrieveDepartments().subscribe((departments: any) => {
      this._departments.next(departments);
    });
  }

  getNextObjects(department: IDepartment, start: number): void {
    this.loadingService.departmentLoading.next(department.departmentId);
    const $objs: Observable<any>[] = [];
    const depts = this._departments.getValue();

    department.objects = [];
    for (let i = start; i < start + 6; i++) {
      const objectId = department.objectData.objectIDs[i];
      $objs.push(this.http.get(`${this.url}/objects/${objectId}`));
    }

    forkJoin($objs)
      .pipe(
        tap((objects) => {
          department.objects = objects;
          depts.map((dept: IDepartment) => {
            if (department.departmentId === dept.departmentId) {
              return department;
            }
            return dept;
          });
          this._departments.next(depts);
        }),
        catchError((err) => {
          return this.pipeError(err, `Objects could not be retrieved for the ${department.displayName} from position ${start}`);
        }),
      )
      .subscribe();
  }

  pipeError(err: any, description: string): Observable<never> {
    console.error(err);
    this.setErrorNotification(description);
    return EMPTY;
  }

  private $retrieveDepartments(): Observable<any> {
    return this.http.get(`${this.url}/departments`).pipe(
      mergeMap((departments: any) => {
        const $depts: Observable<any>[] = [];
        departments.departments.forEach((department: IDepartment) => {
          let objectData: IObjects;
          $depts.push(
            this.http.get(`${this.url}/objects?departmentIds=${department.departmentId}`).pipe(
              mergeMap((objects: any) => {
                objectData = objects;
                const $objs: Observable<any>[] = [];
                (objectData as IObjects).objectIDs.forEach((objectId: number, index2: number) => {
                  if (index2 < 6) {
                    $objs.push(
                      this.http.get(`${this.url}/objects/${objectId}`).pipe(
                        catchError((err) => {
                          return this.pipeError(
                            err,
                            `Object could not be retrieved for Object with ID of ${objectId} within the ${department.displayName} department`,
                          );
                        }),
                      ),
                    );
                  }
                });
                return $objs;
              }),
              combineLatestAll(),
              map((objects) => {
                return {
                  departmentId: department.departmentId,
                  displayName: department.displayName,
                  objectData,
                  objects,
                };
              }),
              catchError((err) => {
                return this.pipeError(err, `Objects information could not be retrieved for ${department.displayName}`);
              }),
            ),
          );
        });
        return $depts;
      }),
      combineLatestAll(),
      catchError((err) => {
        return this.pipeError(err, 'Department information could not be retrieved');
      }),
    );
  }

  private setErrorNotification(description: string): void {
    this.notificationService.setNotification({
      description,
      title: 'Error',
      type: ENotificationType.ERROR,
    });
  }
}
