import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { of as ObservableOf } from 'rxjs';

import { AppComponent } from './app.component';
import { DepartmentComponent } from './components/department/department.component';
import { NotificationComponent } from './components/notification/notification.component';
import { IDepartment } from './models/departments';

describe('AppComponent', () => {
  let app: AppComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [AppComponent, NotificationComponent, DepartmentComponent],
      providers: [
        {
          provide: HttpClient,
          useValue: {
            get: jest.fn().mockReturnValue(ObservableOf({})),
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    const fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;
  });

  describe('ngOnInit()', () => {
    beforeEach(() => {
      (app as any).museumService.retrieveDepartments = jest.fn();
      (app as any).museumService.$getDepartments = jest.fn().mockReturnValue(ObservableOf('some departments'));

      app.ngOnInit();
    });

    it('should call museumService.retrieveDepartments()', () => {
      expect((app as any).museumService.retrieveDepartments).toHaveBeenCalled();
    });

    it('should set departments', () => {
      expect(app.departments).toBe('some departments');
    });
  });

  describe('ngOnDestroy()', () => {
    it('should stop subscriptions', () => {
      (app as any).ngUnsubscribe.next = jest.fn();
      (app as any).ngUnsubscribe.complete = jest.fn();

      app.ngOnDestroy();

      expect((app as any).ngUnsubscribe.next).toHaveBeenCalledWith(true);
      expect((app as any).ngUnsubscribe.complete).toHaveBeenCalled();
    });
  });

  describe('trackBy()', () => {
    it('should return the departmentId', () => {
      const dept: Partial<IDepartment> = {
        departmentId: 22,
      };

      expect(app.trackBy(1, dept as IDepartment)).toBe(22);
    });
  });
});
