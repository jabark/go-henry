import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';

import { IDepartment } from './models/departments';
import { LoadingService } from './services/loading.service';
import { MuseumService } from './services/museum.service';

@Component({
  selector: 'gh-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  departments: IDepartment[] = [];
  isLoading = false;
  departmentLoading = false;
  private ngUnsubscribe = new Subject();

  constructor(private readonly museumService: MuseumService, private readonly loadingService: LoadingService) {
    this.loadingService.departmentLoading.subscribe((departmentLoading) => {
      this.departmentLoading = !!departmentLoading;
    });

    this.loadingService.isLoading.subscribe((isLoading) => {
      this.isLoading = isLoading;
    });
  }

  ngOnInit(): void {
    this.museumService.retrieveDepartments();

    this.museumService
      .$getDepartments()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((departments) => {
        this.departments = departments;
      });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next(true);
    this.ngUnsubscribe.complete();
  }

  trackBy(_index: number, dept: IDepartment) {
    return dept.departmentId;
  }
}
