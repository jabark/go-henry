import { IObject, IObjects } from './objects';

export interface IDepartment {
  departmentId: number;
  displayName: string;
  objectData: IObjects;
  objects: IObject[];
}
