enum EGender {
  MALE = 'Male',
  FEMALE = 'Female',
}

enum EObjectName {
  DRESS = 'Dress',
  PAINTING = 'Painting',
  PHOTOGRAPH = 'Photograph',
  VASE = 'Vase',
}

export interface IObjects {
  total: number;
  objectIDs: number[];
}

export interface IObject {
  objectID: number;
  isHighlight: boolean;
  accessionNumber: string;
  accessionYear: string;
  isPublicDomain: boolean;
  primaryImage: string;
  primaryImageSmall: string;
  additionalImages: string[];
  constituents: IConstituent[];
  department: string;
  objectName: EObjectName;
  title: string;
  culture: string;
  period: string;
  dynasty: string;
  reign: string;
  portfolio: string;
  artistRole: string;
  artistPrefix: string;
  artistDisplayName: string;
  artistDisplayBio: string;
  artistSuffix: string;
  artistAlphaSort: string;
  artistNationality: string;
  artistBeginDate: string;
  artistEndDate: string;
  artistGender: string;
  artistWikidata_URL: string;
  artistULAN_URL: string;
  objectDate: string;
  objectBeginDate: number;
  objectEndDate: number;
  medium: string;
  dimensions: string;
  dimensionsParsed: IDemension[];
  measurements: IMeasurementObj[];
  creditLine: string;
  geographyType: string;
  city: string;
  state: string;
  county: string;
  country: string;
  region: string;
  subregion: string;
  locale: string;
  locus: string;
  excavation: string;
  river: string;
  classification: string;
  rightsAndReproduction: string;
  linkResource: string;
  metadataDate: Date;
  repository: string;
  objectURL: string;
  tags: ITag[];
  objectWikidata_URL: string;
  isTimelineWork: boolean;
  GalleryNumber: string;
}

interface ITag {
  term: string;
  AAT_URL: string;
  Wikidata_URL: string;
}

interface IMeasurementObj {
  elementName: string;
  elementDescription: string;
  elementMeasurements: IMeasurement;
}

interface IMeasurement {
  Height: number;
  Length: number;
  Width: number;
}

interface IDemension {
  element: string;
  dimensionType: string;
  dimension: number;
}

interface IConstituent {
  constituentID: number;
  role: string;
  name: string;
  constituentULAN_URL: string;
  constituentWikidata_URL: string;
  gender: EGender;
}
