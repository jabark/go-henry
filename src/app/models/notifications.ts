export enum ENotificationType {
  NONE = 'None',
  ERROR = 'Error',
  INFO = 'Info',
  SUCCESS = 'Success',
  WARNING = 'Warning',
}

export interface INotification {
  type: ENotificationType;
  title: string;
  description?: string;
  fadeOutTimer?: number;
}
