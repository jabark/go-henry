import { HttpRequest } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { LoadingInterceptor } from './loading.interceptor';

describe('LoadingInterceptor', () => {
  let interceptor: LoadingInterceptor;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoadingInterceptor],
    });

    interceptor = TestBed.inject(LoadingInterceptor);
  });

  describe('intercept()', () => {
    // TODO: write intercept unit test
  });

  describe('removeRequest()', () => {
    it('should remove request from array of requests', () => {
      const req1 = new HttpRequest('GET', 'some://url');
      const req2 = new HttpRequest('GET', 'another://url');
      (interceptor as any).loadingService.isLoading.next = jest.fn();
      (interceptor as any).requests = [req1, req2];

      interceptor.removeRequest(req1);
      expect((interceptor as any).loadingService.isLoading.next).toHaveBeenCalledWith(true);

      interceptor.removeRequest(req2);
      expect((interceptor as any).loadingService.isLoading.next).toHaveBeenCalledWith(false);
    });
  });
});
