import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of as ObservableOf } from 'rxjs';

import { IDepartment } from '../../models/departments';
import { IObject } from '../../models/objects';
import { SearchService } from '../../services/search.service';
import { SearchComponent } from './search.component';

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;
  const searchDepartment: IDepartment = {
    departmentId: -1,
    displayName: 'Search Results for "An artist"',
    objectData: {
      total: 5,
      objectIDs: [9, 19, 22, 44, 56],
    },
    objects: [{ objectID: 9 }, { objectID: 19 }, { objectID: 22 }, { objectID: 44 }, { objectID: 56 }] as IObject[],
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SearchComponent],
      providers: [
        {
          provide: SearchService,
          useValue: {
            $hasSearchDepartment: () => ObservableOf(true),
            $search: () => ObservableOf(searchDepartment),
            updateSearchDepartment: jest.fn(),
            setSearchDepartment: jest.fn(),
            removeSearchDepartment: jest.fn(),
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
  });

  describe('ngOnInit()', () => {
    it('should set hasSearchDepartment', (done) => {
      (component as any).hasSearchDepartment = false;

      component.ngOnInit();

      (component as any).searchService.$hasSearchDepartment().subscribe(() => {
        expect((component as any).hasSearchDepartment).toBe(true);
        done();
      });
    });
  });

  describe('onKeyUp()', () => {
    it('should call clearTimeout with the time', () => {
      const clearTimeoutSpy = jest.spyOn(window, 'clearTimeout');
      (component as any).timer = 'A timer' as any;

      component.onKeyUp();

      expect(clearTimeoutSpy).toHaveBeenCalledWith('A timer');
    });

    it('should call doSearch after 1 second', () => {
      jest.useFakeTimers();
      const doSearchSpy = jest.spyOn(component, 'doSearch');

      component.onKeyUp();

      expect(doSearchSpy).not.toHaveBeenCalled();

      jest.advanceTimersByTime(1000);

      expect(doSearchSpy).toHaveBeenCalled();

      jest.useRealTimers();
    });
  });

  describe('doSearch()', () => {
    describe('given search term is longer than 2', () => {
      beforeEach(() => {
        component.searchTerm = 'lol';
      });

      describe('given we have a search department', () => {
        it('should call updateSearchDepartment with the results', (done) => {
          (component as any).hasSearchDepartment = true;

          component.doSearch();

          (component as any).searchService.$search().subscribe((results: any) => {
            expect((component as any).searchService.updateSearchDepartment).toHaveBeenCalledWith(results);
            done();
          });
        });
      });

      describe('given we do not have a search department', () => {
        it('should call setSearchDepartment with the results', (done) => {
          (component as any).hasSearchDepartment = false;

          component.doSearch();

          (component as any).searchService.$search().subscribe((results: any) => {
            expect((component as any).searchService.setSearchDepartment).toHaveBeenCalledWith(results);
            done();
          });
        });
      });
    });

    describe('given search term is shorter than 3', () => {
      beforeEach(() => {
        component.searchTerm = 'lo';
      });

      afterEach(() => {
        ((component as any).searchService.removeSearchDepartment as jest.SpyInstance).mockReset();
      });

      describe('given we have a search department', () => {
        it('should call removeSearchDepartment', () => {
          (component as any).hasSearchDepartment = true;

          component.doSearch();

          expect((component as any).searchService.removeSearchDepartment).toHaveBeenCalled();
        });
      });

      describe('given we do not have a search department', () => {
        it('should not call removeSearchDepartment', () => {
          (component as any).hasSearchDepartment = false;

          component.doSearch();

          expect((component as any).searchService.removeSearchDepartment).not.toHaveBeenCalled();
        });
      });
    });
  });
});
