import { Component, OnInit } from '@angular/core';

import { SearchService } from '../../services/search.service';

@Component({
  selector: 'gh-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  searchTerm = '';
  private timer!: number;
  private readonly interval = 1000;
  private hasSearchDepartment = false;

  constructor(private readonly searchService: SearchService) {}

  ngOnInit(): void {
    this.searchService.$hasSearchDepartment().subscribe((hasSearchDepartment) => {
      this.hasSearchDepartment = hasSearchDepartment;
    });
  }

  onKeyUp(): void {
    clearTimeout(this.timer);
    this.timer = window.setTimeout(() => this.doSearch(), this.interval);
  }

  doSearch(): void {
    if (this.searchTerm.length > 2) {
      this.searchService.$search(this.searchTerm).subscribe((results) => {
        if (this.hasSearchDepartment) {
          this.searchService.updateSearchDepartment(results);
        } else {
          this.searchService.setSearchDepartment(results);
        }
      });
    } else {
      if (this.hasSearchDepartment) {
        this.searchService.removeSearchDepartment();
      }
    }
  }
}
