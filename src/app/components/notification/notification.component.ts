import { Component, OnInit } from '@angular/core';

import { INotification, ENotificationType } from '../../models/notifications';
import { NotificationService } from '../../services/notification.service';
@Component({
  selector: 'gh-notification',
  styleUrls: ['./notification.component.scss'],
  templateUrl: './notification.component.html',
})
export class NotificationComponent implements OnInit {
  notification!: INotification;
  visibilityTimeout = false;
  notificationType = ENotificationType;

  constructor(private readonly notificationService: NotificationService) {}

  ngOnInit(): void {
    this.notificationService.notification.subscribe((notification) => {
      this.notification = notification;

      if (notification.fadeOutTimer) {
        setTimeout(() => {
          this.visibilityTimeout = true;
        }, notification.fadeOutTimer * 1000);

        setTimeout(() => {
          this.removeNotification();
          this.visibilityTimeout = false;
        }, notification.fadeOutTimer * 1000 + 1000);
      }
    });
  }

  get notificationClass(): string {
    switch (this.notification.type) {
      case ENotificationType.ERROR:
        return 'gh-notification--error';
      case ENotificationType.WARNING:
        return 'gh-notification--warning';
      case ENotificationType.INFO:
        return 'gh-notification--info';
      case ENotificationType.SUCCESS:
        return 'gh-notification--success';
      default:
        return 'is-hidden';
    }
  }

  removeNotification(): void {
    this.notificationService.removeNotification();
  }
}
