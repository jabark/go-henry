import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of as ObservableOf } from 'rxjs';

import { INotification, ENotificationType } from '../../models/notifications';
import { NotificationService } from '../../services/notification.service';
import { NotificationComponent } from './notification.component';

describe('NotificationComponent', () => {
  let notificationService: NotificationService;
  let component: NotificationComponent;
  let fixture: ComponentFixture<NotificationComponent>;

  const testNotification: INotification = {
    title: 'Error',
    type: ENotificationType.ERROR,
  };
  const mockNotificationService = {
    notification: ObservableOf(testNotification),
    removeNotification: jest.fn(),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NotificationComponent],
      providers: [
        {
          provide: NotificationService,
          useValue: mockNotificationService,
        },
      ],
    }).compileComponents();

    notificationService = TestBed.inject(NotificationService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationComponent);
    component = fixture.componentInstance;
  });

  describe('ngOnInit()', () => {
    describe('notificationService.notification()', () => {
      beforeEach(() => {
        jest.useFakeTimers();
      });

      afterEach(() => {
        jest.useRealTimers();
      });

      it('should set the notification', () => {
        (notificationService.notification as any) = ObservableOf(testNotification);
        component.notification = testNotification;

        component.ngOnInit();

        notificationService.notification.subscribe(() => {
          expect(component.notification).toEqual(testNotification);
        });
      });

      describe('given fadeOutTimer has been supplied', () => {
        beforeEach(() => {
          component.removeNotification = jest.fn();
          (notificationService.notification as any) = ObservableOf({
            ...testNotification,
            fadeOutTimer: 2,
          });
          component.ngOnInit();
        });

        it('should set fadeOut to true after the supplied second count', () => {
          expect(component.visibilityTimeout).toBe(false);
          jest.advanceTimersByTime(2001);
          expect(component.visibilityTimeout).toBe(true);
        });

        it('should set fadeOut to false and call removeNotification a second after', () => {
          expect(component.visibilityTimeout).toBe(false);
          jest.advanceTimersByTime(3001);
          expect(component.visibilityTimeout).toBe(false);
          expect(component.removeNotification).toHaveBeenCalled();
        });
      });

      describe('given fadeOutTimer has not been supplied', () => {
        it('should not set fadeOut to true', () => {
          (notificationService.notification as any) = ObservableOf(testNotification);
          component.ngOnInit();

          expect(component.visibilityTimeout).toBe(false);
          jest.advanceTimersByTime(2001);
          expect(component.visibilityTimeout).toBe(false);
        });
      });
    });
  });

  describe('get notificationClass()', () => {
    it('should return the correct class string', () => {
      component.notification = {
        ...testNotification,
        type: ENotificationType.ERROR,
      };
      expect(component.notificationClass).toBe('gh-notification--error');

      component.notification = {
        ...testNotification,
        type: ENotificationType.WARNING,
      };
      expect(component.notificationClass).toBe('gh-notification--warning');

      component.notification = {
        ...testNotification,
        type: ENotificationType.INFO,
      };
      expect(component.notificationClass).toBe('gh-notification--info');

      component.notification = {
        ...testNotification,
        type: ENotificationType.SUCCESS,
      };
      expect(component.notificationClass).toBe('gh-notification--success');

      component.notification = {
        ...testNotification,
        type: ENotificationType.NONE,
      };
      expect(component.notificationClass).toBe('is-hidden');
    });
  });

  describe('removeNotification()', () => {
    it('should call removeNotification', () => {
      component.removeNotification();

      expect(notificationService.removeNotification).toHaveBeenCalled();
    });
  });
});
