import { Component, DoCheck, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { IDepartment } from '../../models/departments';
import { LoadingService } from '../../services/loading.service';
import { MuseumService } from '../../services/museum.service';
import { DepartmentModalComponent } from './department-modal/department-modal.component';

@Component({
  selector: 'gh-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.scss'],
})
export class DepartmentComponent implements OnInit, DoCheck {
  @Input() department!: IDepartment;

  currentPage = 1;
  lastPage = 1;
  isLoading = false;
  departmentLoading = false;

  private oldDepartment!: IDepartment;

  constructor(
    private readonly museumService: MuseumService,
    private readonly loadingService: LoadingService,
    private readonly dialog: MatDialog,
  ) {}

  ngOnInit() {
    this.loadingService.departmentLoading.subscribe((departmentLoading) => {
      this.departmentLoading = departmentLoading === this.department.departmentId;
    });

    this.loadingService.isLoading.subscribe((isLoading) => {
      this.isLoading = isLoading;
    });
  }

  ngDoCheck() {
    if (JSON.stringify(this.department) !== JSON.stringify(this.oldDepartment)) {
      this.oldDepartment = this.department;
      if (this.department.objectData) {
        this.lastPage = Math.ceil(this.department.objectData.total / 6);
      }
    }
  }

  getNextObjects() {
    this.museumService.getNextObjects(this.department, (this.currentPage - 1) * 6);
  }

  openDialog(objectId: number): void {
    this.dialog.open(DepartmentModalComponent, {
      width: '80%',
      data: this.department.objects.find((object) => {
        return object.objectID === objectId;
      }),
    });
  }
}
