import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { of as ObservableOf } from 'rxjs';

import { LoadingService } from '../../services/loading.service';
import { MuseumService } from '../../services/museum.service';
import { DepartmentModalComponent } from './department-modal/department-modal.component';
import { DepartmentComponent } from './department.component';

describe('DepartmentComponent', () => {
  let component: DepartmentComponent;
  let fixture: ComponentFixture<DepartmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DepartmentComponent],
      imports: [MatDialogModule],
      providers: [
        {
          provide: MuseumService,
          useValue: {},
        },
        {
          provide: LoadingService,
          useValue: {
            departmentLoading: ObservableOf(22),
            isLoading: ObservableOf(true),
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentComponent);
    component = fixture.componentInstance;
  });

  describe('ngOnInit()', () => {
    it('should set up departmentLoading subscription', () => {
      component.department = {
        departmentId: 22,
      } as any;
      expect(component.departmentLoading).toBe(false);

      component.ngOnInit();

      (component as any).loadingService.departmentLoading.subscribe(() => {
        expect(component.departmentLoading).toBe(true);
      });
    });

    it('should set up isLoading subscription', () => {
      expect(component.isLoading).toBe(false);

      component.ngOnInit();

      (component as any).loadingService.isLoading.subscribe(() => {
        expect(component.isLoading).toBe(true);
      });
    });
  });

  describe('ngDoCheck()', () => {
    describe('given we have been given a new department', () => {
      it('should set the last page number', () => {
        component.department = {
          departmentId: 22,
          objectData: {
            total: 22,
          },
        } as any;
        (component as any).oldDepartment = {
          departmentId: 1337,
          objectData: {
            total: 1337,
          },
        } as any;

        component.ngDoCheck();

        expect(component.lastPage).toBe(4);
      });
    });

    describe('given we have not been given a new department', () => {
      it('should not set the last page number', () => {
        component.lastPage = 22;
        component.department = {
          departmentId: 22,
          objectData: {
            total: 22,
          },
        } as any;
        (component as any).oldDepartment = {
          departmentId: 22,
          objectData: {
            total: 22,
          },
        } as any;

        component.ngDoCheck();

        expect(component.lastPage).toBe(22);
      });
    });
  });

  describe('getNextObjects()', () => {
    it('should call getNextObjects with correct params', () => {
      component.department = 'A department' as any;
      component.currentPage = 3;
      (component as any).museumService.getNextObjects = jest.fn();

      component.getNextObjects();

      expect((component as any).museumService.getNextObjects).toHaveBeenCalledWith('A department', 12);
    });
  });

  describe('openDialog()', () => {
    it('should call dialog.open with correct params', () => {
      component.department = {
        objects: [
          {
            objectID: 22,
          },
          {
            objectID: 1337,
          },
        ],
      } as any;
      (component as any).dialog.open = jest.fn();

      component.openDialog(22);

      expect((component as any).dialog.open).toHaveBeenCalledWith(DepartmentModalComponent, {
        width: '80%',
        data: {
          objectID: 22,
        },
      });
    });
  });
});
