import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { IObject } from '../../../models/objects';

@Component({
  selector: 'gh-department-modal',
  templateUrl: 'department-modal.component.html',
  styleUrls: ['department-modal.component.scss'],
})
export class DepartmentModalComponent {
  constructor(public dialogRef: MatDialogRef<DepartmentModalComponent>, @Inject(MAT_DIALOG_DATA) public data: IObject) {}

  close(): void {
    this.dialogRef.close();
  }
}
